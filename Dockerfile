# Use CentOS 8 Stream base image
FROM gitlab-registry.cern.ch/linuxsupport/cs8-base:20210519-1

LABEL maintainers="Ignacio Coterillo <icoteril@cern.ch>"

USER root

RUN dnf -y install python3 && pip3 install pip --upgrade && \
    pip install flake8 pylint bandit twine pylint-venv

COPY .pylintrc /etc/pylintrc

CMD ["/bin/bash"]
