# python-ci

[[_TOC_]]

Include in your Python project to lint the code and (optionally) publish the code to PyPI.

Example of using python-ci along with rpm-ci:
```yaml
---
variables:
  # Python CI
  SRC: 'cerndb'
  PYPI: 'True'
  # RPMCI
  KOJI_TAG: 'db'
  BUILD_7: 'True'
  DIST_7: '.el7.cern'
  BUILD_8: 'True'


include:
  - 'https://gitlab.cern.ch/db/python-ci/-/raw/master/python-ci.yml'
  - 'https://gitlab.cern.ch/linuxsupport/rpmci/raw/master/rpm-ci.yml'

```

# Environment variables

A set of env variables can be set to modify the behavior of the Python linter.

| Variable | Default value | Allowed Values | Description |
| --- | --- | --- | --- |
| PRE_LINT_INSTALL | 'True' | 'True', 'False' | Run `pip install .` before linting.

#### Variables in detail
- `PRE_LINT_INSTALL` - added to install dependencies defined in `setup.py`. Then, we can detect missing dependencies with pylint, for example: `E0401: Unable to import 'confuse' (import-error)`

# Linters

## pylint-venv

We use [pylint-venv](https://pypi.org/project/pylint-venv/) to use the global installation of `pylint` but install the tested package inside a new virtual environment.


## .pylintrc

A `pylintrc` file can be placed in your repository root directory. It can be useful if you want to disable certain errors and messages.

To generate a default pylint file, run:
```
pylint --generate-rcfile > .pylintrc
```

There is a default `pylintrc` file in the `python-ci` image. It is placed in `/etc/pylintrc` and is read only if you don't have a custom file in your repository.
Generally, pylint tries to read the rc file from several locations and uses the first one it finds. This is described [here.](http://pylint.pycqa.org/en/latest/user_guide/run.html#command-line-options)
